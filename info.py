INFO = '''INFO
     This is a simple screenshot manager which
     automates taking specified number of
     screenshots in desired time interval.
     For more info on each of the functions,
     hover over their labels to the left.
     To start capturing, press "Space bar"
     '''

INTERVAL = '''How long to
take screenshots
(in seconds).'''

SCREENSHOTS = '''How many captures
to take per
interval'''

ORIGINATION_X = '''Abscissa of the
origin point'''

ORIGINATION_Y = '''Ordinate of the
origin point'''

WIDTH = '''Width of the screen to
capture, starting from
the origin point.'''

HEIGHT = '''Height of the screen
to capture, starting
from the origin point.'''

DELAY = '''How long to wait
before capture starts
(in seconds).'''

PATH = '''Path of the location
where images will
be saved.'''

PARAMS = {'Interval': INTERVAL, 'Screenshots': SCREENSHOTS, 'Origination_x': ORIGINATION_X,
          'Origination_y': ORIGINATION_Y, 'Width': WIDTH, 'Height': HEIGHT, 'Delay': DELAY,
          'Path': PATH}

