from __future__ import division
import Tkinter as Tk
import ttk
import tkMessageBox
from time import sleep
import threading
from datetime import datetime
import os
from Queue import Queue
from Info import PARAMS, INFO
from pyrobot import Robot
import pythoncom
import pyHook
import json

NEW_THREAD = False  # Used for killing running threads

# Which monitor to use
MONITORS = Robot().get_display_monitors()
if len(MONITORS) > 1:
    MONITOR = MONITORS[1]
else:
    MONITOR = MONITORS[0]


class GUI(object):
    def __init__(self, root, labels):
        self.entries_dict = {}
        self.root = root
        self.root.title('Screenshot Master')
        self.root.minsize(width=470, height=175)
        self.root.resizable(width=Tk.FALSE, height=Tk.FALSE)

        # Build all labels and entries
        for index, label in enumerate(labels):
            lab = Tk.Label(self.root, text=label)
            lab_info = PARAMS[label]
            lab.grid(row=index)
            lab.bind('<Enter>',
                     lambda event, li=lab_info: self.on_enter(event, li))
            lab.bind('<Leave>', self.on_leave)
            entry = Tk.Entry(self.root)
            entry.grid(row=index, column=1)
            self.entries_dict[label] = entry

        # Insert default values in entries
        if os.path.isfile('defaults.json'):
            with open('defaults.json', 'r') as f:
                defaults = json.load(f)
            for label, entry in defaults.items():
                self.entries_dict[label].insert(0, entry)

        Tk.Label(self.root, text=INFO).grid(row=0, column=2, rowspan=6)
        Tk.Label(self.root, text='Screenshots progress').grid(row=6, column=2)
        self.progress_bar = None
        self.entered = None
        self.thread = None
        self.threads = []
        self.progress_queue = Queue()
        self.keylogger_queue = Queue()

        # Keylogger to catch trigger when GUI is not in focus
        hm = pyHook.HookManager()
        hm.KeyDown = self.keylogger_thread
        hm.HookKeyboard()
        pythoncom.PumpWaitingMessages()

        self.check_for_spacebar()

    # Start thread for key press
    def keylogger_thread(self, event):
        pyhook_thread = threading.Thread(target=self.check_input,
                                         args=(event,))
        pyhook_thread.daemon = True
        pyhook_thread.start()
        return True

    # If key pressed is space bar, put in queue
    def check_input(self, event):
        if str(event.Ascii) == '32':
            self.keylogger_queue.put('kek')

    # Periodically check queue for pressed space bar
    # If queue is not empty, call trigger method
    def check_for_spacebar(self):
        while self.keylogger_queue.qsize():
            self.keylogger_queue.get(0)
            self.trigger()
        self.root.after(100, self.check_for_spacebar)

    # Show text when hovering over labels
    def on_enter(self, event, lab_info):
        self.entered = Tk.Label(self.root, text=lab_info, bg='#ffffb3')
        self.entered.grid(row=0, rowspan=3, column=1)

    # Destroy text when exiting labels
    def on_leave(self, event):
        self.entered.destroy()

    # Check if values in entries are of correct type
    def check_types(self):
        for key, val in self.entries_dict.items():
            if key == 'Path':
                if not val.get() or not os.path.isdir(val.get().strip()):
                    return False
            else:
                if not self.check_num(val):
                    return False
        return True

    # Check that non Path entries have floating point values
    @staticmethod
    def check_num(entry):
        if not entry.get():
            return False
        try:
            float(entry.get())
        except ValueError:
            return False
        else:
            return True

    # Method called when space bar is pressed
    def trigger(self):
        if not self.check_types():
            tkMessageBox.showerror('Error',
                                   'Check that no entry is empty\n'
                                   'and that Path exists')
        else:
            self.take_screenshots()

    # General screenshot logic
    def take_screenshots(self):
        global NEW_THREAD
        NEW_THREAD = True
        if self.threads:
            self.threads[0].join()  # Waits for thread to finish
            self.threads.pop(0)
        max_bar_length = float(self.entries_dict['Interval'].get()) + 0.01
        self.progress_bar = ttk.Progressbar(self.root, orient=Tk.HORIZONTAL,
                                            length=150,
                                            mode='determinate',
                                            maximum=max_bar_length)
        self.progress_bar.grid(row=7, column=2)

        # Spawn thread for capturing screenshots
        self.thread = ThreadedCapture(
            int(self.entries_dict['Origination_x'].get()),
            int(self.entries_dict['Origination_y'].get()),
            int(self.entries_dict['Width'].get()),
            int(self.entries_dict['Height'].get()),
            int(self.entries_dict['Screenshots'].get()),
            float(self.entries_dict['Interval'].get()),
            float(self.entries_dict['Delay'].get()),
            self.entries_dict['Path'].get(),
            self.progress_queue, self.progress_bar)
        self.thread.daemon = True  # Ensures thread dies when program exits
        self.threads.append(self.thread)
        self.thread.start()

        self.periodic_call()

    # Call yourself periodically
    def periodic_call(self):
        self.check_progress()
        if self.thread.is_alive():
            self.root.after(100, self.periodic_call)
        else:
            self.progress_bar['value'] = 0

    # Fill up progress bar for every screenshot
    def check_progress(self):
        while self.progress_queue.qsize():
            self.progress_queue.get(0)  # If empty, don't block thread
            try:
                self.progress_bar.step(
                    float(self.entries_dict['Interval'].get()) /
                    float(self.entries_dict['Screenshots'].get()))
            except ValueError:
                self.progress_bar['maximum'] = 0


class ThreadedCapture(threading.Thread):
    def __init__(self, x, y, w, h, screens, interval, delay, path, queue,
                 progress):
        threading.Thread.__init__(self)
        self.x = x
        self.y = y
        self.w = w
        self.h = h
        self.screens = screens
        self.interval = interval
        self.delay = delay
        self.path = path
        self.progress_queue = queue
        self.progress_bar = progress

    # Start capturing images
    def run(self):
        global NEW_THREAD
        NEW_THREAD = False
        screenshots_list = []
        take_every = 0
        try:
            take_every = self.interval / self.screens
        except ZeroDivisionError:
            pass
        sleep(self.delay)
        for i in range(self.screens):
            if not NEW_THREAD:  # If none new threads have spawned
                try:
                    img = Robot().take_screenshot(
                        (MONITOR[0] + self.x, MONITOR[1] + self.y,
                         MONITOR[0] + self.w, MONITOR[1] + self.h))
                except ValueError:
                    return
                self.progress_queue.put('done')
                screenshots_list.append(img)
                sleep(take_every)
            else:
                return
        for i, img in enumerate(screenshots_list):
            now = datetime.now().strftime('%Y%m%d-%H%M%S-') + str(i).zfill(2)
            img.save('{0}\image_{1}.jpeg'.format(self.path.strip(), now))


def on_closing():
    default_values = {}
    for label, entry in gui.entries_dict.items():
        default_values[label] = entry.get()
    with open('defaults.json', 'w') as f:
        json.dump(default_values, f, indent=4)
    tk.quit()

if __name__ == '__main__':
    tk = Tk.Tk()
    gui_labels = ['Interval', 'Screenshots', 'Origination_x', 'Origination_y',
                  'Width', 'Height', 'Delay', 'Path']
    gui = GUI(tk, gui_labels)
    tk.protocol("WM_DELETE_WINDOW", on_closing)
    tk.mainloop()
